﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class What_To_Use : MonoBehaviour
{
    public Movement move;
    public flight fly;
    private Vector2 moveUp;
    private Rigidbody rig;
    private Animator anim;
    private Quaternion pRot;
    private CharacterController pcont;
    private PlayerAttributes pa;

    // Start is called before the first frame update
    void Awake()
    {
        move.enabled = true;
        fly.enabled = false;
        rig = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        pcont = GetComponent<CharacterController>();
        pRot = transform.rotation;
        pa = GetComponent<PlayerAttributes>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fly") && rig.useGravity == true)
        {
            anim.SetBool("g_anim", false);
            anim.SetBool("flight_anim", true);
            move.enabled = false;
            fly.enabled = true;
            rig.useGravity = false;
            moveUp.y += 20f;
            pcont.Move(moveUp * Time.deltaTime);
            pa.swapFlightIcon();

        }
        else if (Input.GetButtonDown("Fly") && rig.useGravity == false)
        {
            move.enabled = true;
            fly.enabled = false;
            rig.useGravity = true;
            //anim.SetBool("flight", false);
            anim.SetBool("g_anim", true);
            anim.SetBool("flight_anim", false);
            transform.rotation = new Quaternion(pRot.x, transform.rotation.y, pRot.z, transform.rotation.w);
            pa.swapFlightIcon();
        }
    }

}
