﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class textAlert : MonoBehaviour
{
	public CanvasGroup group;
	public Text txt;
	
	private float sec;
	private float del;

    // Start is called before the first frame update
    void Start()
    {
        group = GetComponent<CanvasGroup>();
    	group.alpha = 0;
    }

    // Update is called once per frame
    void Update()
    {	
    	del -= Time.deltaTime;
    	if (group.alpha == 0 && del <= 0)
    	{
    		group.alpha = 1;
    	}
    	sec -= Time.deltaTime;
    	// If the canvas group is visible, invert alpha after sec amount of time
    	if (group.alpha == 1 && sec <= 0)
    	{
    		group.alpha = 0;
    	}
    }

    // Call to activate the textbox for t secs
    public void showText (string str, float t, float delay)
    {
    	txt.text = str;
    	sec = t;
    	if (del != 0)
    	{
	    	group.alpha = 1;
	    }
	    else
	    {
	    	sec += delay;
    		del = delay;
    	}
    }
}
