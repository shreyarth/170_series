﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gizmo : MonoBehaviour
{
	public float gizSiz = .75f;
	public Color gizColor = Color.yellow;

    void OnDrawGizmos()
    {
    	Gizmos.color = gizColor;
    	Gizmos.DrawWireSphere(transform.position, gizSiz);
    }
}
