﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckGuide : MonoBehaviour
{
	GameObject player;
	[Range(0.0f, 5.0f)]
	public float checkRadius = 2.5f;

    // Start is called before the first frame update
    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance(player.transform.position, transform.position);
        if(distance < checkRadius) Destroy(this.gameObject);
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, checkRadius);
    }
}