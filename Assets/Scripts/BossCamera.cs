﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossCamera : MonoBehaviour
{
    Transform Boss;

    public GameObject tbox;
    textbox tb;

    void Awake()
    {   
        Boss = GameObject.FindGameObjectWithTag("Boss").transform;  // Find boss
        tb = tbox.GetComponent<textbox>();
    }

    void Start()
    {
        tb.callCaption("Chase Hatred and Save Hostages!", 6.0f, new Vector2(0f, -150f));
        tbox.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        //Swtich view

        //transform.position = Boss.transform.position;//make the camera go with player
        //transform.Translate(Vector3.forward * 15);//set it behind player
        transform.LookAt(Boss);
    }
}
