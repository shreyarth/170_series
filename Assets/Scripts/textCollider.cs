using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class textCollider : MonoBehaviour
{
    public GameObject textbox;
    public string caption;
    public float onTime;
    public Vector2 boxPos;

    private bool triggered = false;
	GameObject player;
    textbox tb;

    // Start is called before the first frame update
    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");  // Find player object
        tb = textbox.GetComponent<textbox>();
    }

    void Start()
    {

    }

    void OnTriggerEnter (Collider other)
    {
        if(other.gameObject.CompareTag("Player") && !triggered)
        {
            tb.callCaption(caption, onTime, boxPos);
            textbox.SetActive(true);
            triggered = true;
        }
    }
}
