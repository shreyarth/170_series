﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Cutscene : MonoBehaviour
{
    public void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void ToToot()
    {
        relockCursor();
        SceneManager.LoadScene("TOOT");
    }
    public void toConstruction2()
    {
        relockCursor();
        SceneManager.LoadScene("constructionscene2");
    }
    public void toConstruction3()
    {
        relockCursor();
        SceneManager.LoadScene("construction3");
    }
    public void toConstruction4()
    {
        relockCursor();
        SceneManager.LoadScene("construction4");
    }

    public void CutsceneTo2()
    {
        SceneManager.LoadScene("Cutscene2");
    }
    public void CutsceneTo6()
    {
        SceneManager.LoadScene("Cutscene6");
    }
    public void toCredits()
    {
        SceneManager.LoadScene("Credits");
    }
    /*public void CutsceneTo3()
    //{
    //    SceneManager.LoadScene("constructionscene2");
    //}
    //public void CutsceneTo4()
    //{
        SceneManager.LoadScene("constructionscene2");
    }
    public void CutsceneTo5()
    {
        SceneManager.LoadScene("constructionscene2");
    }
    public void CutsceneToEnding()
    {
        SceneManager.LoadScene("EndCustscene");
    }
    public void CutsceneEnd()
    {
        SceneManager.LoadScene("End");
    }
    public void EndtoMap()
    {
        SceneManager.LoadScene("Map");
    }
    */

    void relockCursor()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
}
