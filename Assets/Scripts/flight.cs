﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flight : MonoBehaviour
{
    [SerializeField]
    private CharacterController pcont;
    [SerializeField]
    private Rigidbody rig;
    #region player vars
    [Header("PLayer Values")]
    [SerializeField]
    private float mouse_x;
    [SerializeField]
    public float mouse_y;
    [SerializeField]
    private float horizontal;
    [SerializeField]
    private float vertical;
    [SerializeField]
    private float baseSpeed = 3f;
    [SerializeField]
    private float rotSpeedX = 1f;
    [SerializeField]
    private float rotSpeedY = 1f;
    [SerializeField]
    private Vector3 moveVec;
    [SerializeField]
    private float AmbientSpeed = 100f;
    [SerializeField]
    private float RotationSpeed = 40f;
    [SerializeField]
    private Vector3 moveDir;
    [SerializeField]
    private Animator anim;
    [SerializeField]
    private Quaternion pRot;
    [SerializeField]
    private float superSpeed;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        rig = GetComponent<Rigidbody>();
        pcont = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();
        pRot = transform.rotation;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        mouse_x = Input.GetAxis("Mouse X");
        mouse_y = Input.GetAxis("Mouse Y");
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");
        anim.SetFloat("velo_x", horizontal);
        anim.SetFloat("velo_y", vertical);
        Quaternion AddRot = Quaternion.identity;
        float roll = 0;
        float pitch = 0;
        float yaw = 0;
        roll = mouse_x * (Time.fixedDeltaTime * RotationSpeed);
        pitch = mouse_y * (Time.fixedDeltaTime * RotationSpeed);
        yaw = vertical * (Time.fixedDeltaTime * RotationSpeed);
        if (pitch > 270 && pitch < 30)
        {
            pitch = 45f;
        }

        if (Input.GetButton("Superspeed"))
        {
            moveDir = transform.TransformDirection(Vector3.forward) * vertical * superSpeed;
        }
        else
        {
            moveDir = transform.TransformDirection(Vector3.forward) * vertical * baseSpeed;
        }

        if(pcont.velocity.magnitude > 0f)
        {

            transform.Rotate(-pitch, roll, 0f);
        }
        else
        {
            float horizontalTurn = Input.GetAxis("Mouse X") * 3f;
            transform.Rotate(0, horizontalTurn, 0);
        }
        pcont.Move(moveDir * Time.deltaTime);

    }
}
