using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float lifespan = 2.0f;
    public float speed = 10f;
    public int damage = 10;

    GameObject player;
    Transform playerPos;
    PlayerAttributes pa;
    Rigidbody rigidbody;
    GameObject pivot;

    private Vector3 dir;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");  // Find player object
        playerPos = GameObject.FindGameObjectWithTag("Player").transform;  // Store player transform
        pa = player.GetComponent<PlayerAttributes>();
        rigidbody = GetComponent<Rigidbody>();
        // pivot = transform.GetChild(0).gameObject;
    }

    void Start()
    {
        
    }

    // When bullet is enabled, set bullet to call Destroy function after lifespan
    void OnEnable()
    {
        rigidbody.velocity = transform.forward * speed;
        this.transform.Rotate(0, 90, 0);
        // transform.LookAt(playerPos);
        // Debug.Log(transform.position - playerPos.position);
        // transform.rotation = Quaternion.LookRotation(pivot.transform.position - playerPos.position, pivot.transform.up);
        // Debug.Log(transform.rotation);
        dir.x = playerPos.position.x;  // Save last seen player position
        dir.y = playerPos.position.y + 1;  // Save last seen player position
        dir.z = playerPos.position.z;  // Save last seen player position
        Invoke ("Destroy", lifespan);
    }

    // Update is called once per frame
    void Update()
    {
        // transform.position = Vector3.MoveTowards(transform.position, dir, speed);
        // transform.rotation = Quaternion.LookRotation(dir); 
        //transform.rotation = Quaternion.LookRotation(rigidbody.velocity);
    }

    void OnTriggerEnter(Collider other)
    {
        // On collision with player, destroy
        if(other.gameObject.tag == "Player")
        {
            pa.damage(damage);
            gameObject.SetActive(false);
        }
    }

    void OnControllerColliderHit(ControllerColliderHit other)
    {
        // On collision with player, destroy
        if(other.gameObject.tag == "Player")
        {
            pa.damage(damage);
        }

        gameObject.SetActive(false);
    }

    // Instead of destroying the object, set it inactive
    void Destroy()
    {
        gameObject.SetActive(false);
    }

    // Don't call invoke when inactive
    void OnDisable()
    {
        CancelInvoke();
    }

    // To set new lifesapn outside of script, call this function
    public void setLifespan (float newLifespan)
    {
        lifespan = newLifespan;
    }

    // To set new damage value outside of script, call this function
    public void setDamage (int newDmg)
    {
        damage = newDmg;
    }
}
