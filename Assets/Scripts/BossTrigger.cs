using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BossTrigger : MonoBehaviour
{
        void OnTriggerEnter (Collider other)
    {
        if(other.gameObject.CompareTag("Player") )
        {
            StartCoroutine(BossFightDelay());
        }
    }
        private IEnumerator BossFightDelay()
        {
            yield return new WaitForSeconds(3f);
            SceneManager.LoadScene("HatredFight");
        }
}
