﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bosstrap : MonoBehaviour
{
    GameObject boss;
    int ranNumber;
    int ranNumberH;
    float ranNumberX;
    float ranNumberXH;
    float ranNumberZ;
    float ranNumberZH;
    // Start is called before the first frame update
    void Start()
    {
        //trap position
        gameObject.transform.position = new Vector3(gameObject.transform.position.x, 44, gameObject.transform.position.z );

        //random value
        ranNumberH = Random.Range(0, 2);
        if (ranNumberH == 0)
        {
            ranNumberXH = -15;
        }
        else
        {
            ranNumberXH = 15;
        }

        ranNumberH = Random.Range(0, 2);
        if (ranNumber == 0)
        {
            ranNumberZH = -15;
        }
        else
        {
            ranNumberZH = 15;
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnControllerColliderHit(ControllerColliderHit collision)
    {
        Debug.Log(collision.collider.gameObject.name);
        Debug.Log(collision.collider.gameObject.tag);
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("111111");
            //if player step on trap, he will be tp to a random place in the air
            collision.gameObject.transform.position = new Vector3(collision.gameObject.transform.position.x+ranNumberXH, collision.gameObject.transform.position.y+5, collision.gameObject.transform.position.z+ranNumberZH);
            Destroy(gameObject); 
        }
    }
}
