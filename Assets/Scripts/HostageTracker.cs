﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HostageTracker : MonoBehaviour
{
	private static Dictionary<string, bool> tracker;

	public Text showCount;
	int counter;
    public int goalCount;

    public Animator hoe_anim;
    public float onTime;

    void Awake()
    {

    }

    // Start is called before the first frame update
    void Start()
    {
        hoe_anim.SetBool("go_in", true);
        StartCoroutine("Hostage_UI");
        if (tracker == null)
        {
            tracker = new Dictionary<string, bool>();

            foreach(GameObject obj in GameObject.FindGameObjectsWithTag("Hostages"))
            {
                tracker.Add(obj.name, false);
            }
        }

        counter = tracker.Count;

        showCount.text = counter.ToString();
    }

    public void setValueTrue(string name)
    {
        tracker[name] = true;
        hoe_anim.SetBool("go_in", true);
        counter--;
        showCount.text = counter.ToString();
        StartCoroutine("Hostage_UI");
    }

    public bool checkValid()
    {
        int temp = goalCount;
        for(int i = 1; i <= tracker.Count; ++i)
        {
            string key = "Hostage" + i;
            if(tracker[key])   --temp;
        }

        if(temp <= 0) return true;
        else    return false;
    }

    IEnumerator Hostage_UI()
    {
        yield return new WaitForSeconds(onTime);
        hoe_anim.SetBool("go_in", false);
    }
}
