﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollBear : MonoBehaviour
{
    public int hp;
    // Start is called before the first frame update
    void Start()
    {
        SetRigidbodyState(true);
        SetColliderState(false);
    }

    // Update is called once per frame
    void Update()
    {
        hp = GetComponent<EnemyPowerful>().health;
        if (hp <= 0)
        {
            GetComponent<EnemyPowerful>().enabled = false;
            Invoke("Die", 0.1f);
        }
    }

    void SetRigidbodyState(bool state)
    {
        Rigidbody[] bodies = GetComponentsInChildren<Rigidbody>();
        foreach (Rigidbody rb in bodies)
        {
            rb.isKinematic = state;
        }

        GetComponent<Rigidbody>().isKinematic = !state;
    }

    void SetColliderState(bool state)
    {
        Collider[] colliders = GetComponentsInChildren<Collider>();
        foreach (Collider cl in colliders)
        {
            cl.enabled = state;
        }

        GetComponent<Collider>().enabled = !state;
    }
    void Die()
    {
        GetComponent<EnemyPowerful>().enabled = false;
        GetComponent<Animator>().enabled = false;
        SetRigidbodyState(false);
        SetColliderState(true);
        
        //GetComponent<PandaBehaviour>().enabled = false;
        Destroy(gameObject, 5);
    }
}
