﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyJet : MonoBehaviour
{
    Transform player;
    float distance;
    Vector3 randomDirection;
    float flyTimer = 1.0f;
    float moveTimer = 2.0f;
    // Start is called before the first frame update
    void Start()
    {
        //initial direction
        randomDirection = new Vector3(Random.Range(-359, 359), 0, Random.Range(-359, 359));
    }
    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;//find player

    }
    // Update is called once per frame
    void Update()
    {
        Vector3 direction = player.position - transform.position;//find direction to player
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), 0.3f);//Rotate to player
        
        distance = Vector3.Distance(player.transform.position, transform.position);//check distance

        if (distance < 40)
        {//enable timer for moving on Y axis
            flyTimer -= Time.deltaTime;
        }
        else
        {// enable timer for common movement
            moveTimer -= Time.deltaTime;        
        }

        if (moveTimer <= 0)
        {//common movement
            randomDirection = new Vector3(Random.Range(-359, 359), 0, Random.Range(-359, 359));
            moveTimer = 2.0f;
        }

        if (flyTimer <= 0)
        {// escaping on Y axis
            randomDirection = new Vector3(Random.Range(-359, 359), Random.Range(-359, 359), Random.Range(-359, 359));
            flyTimer = 1.0f;
        }    

        transform.position = Vector3.MoveTowards(transform.position, randomDirection, 0.1f);// movement during gameplay
        Debug.Log(randomDirection);

        //Set bounds for flying
        if (transform.position.y <= -20)
        {// keep away from the ground
            transform.position = new Vector3(transform.position.x, -20, transform.position.z);
        }

        if (transform.position.y >= 120)
        {// aovid flying too high
            transform.position = new Vector3(transform.position.x, 120, transform.position.z);
        }
    }
}
