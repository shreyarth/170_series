﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

using Panda;

public class EnemyPowerful : MonoBehaviour
{
    Transform player;
    public GameObject punching1;
    public GameObject playerPunch;
    float distance;
    public GameObject deathVFX;
    public int health = 3;

    [Range(0.0f, 15.0f)]
    public float idleDistance = 10.0f;
    [Range(0.0f, 5.0f)]
    public float attackDistance = 2.0f;

    NavMeshAgent agent;
    Vector3 destination;
    public Animator bear_anim;
    public GameObject expo; 

    ////////////////////////// Behavior tree objs & methods ////////////////////////////
    bool _isIdle = true;
    [Task]
    bool isIdle
    {
        get
        {
            return distance > idleDistance;
        }
    }

    bool _isRange = true;
    [Task]
    bool isRange
    {
        get
        {
            return distance < attackDistance;
        }
    }

    [Task]
    void disablePunch()
    {
        if(punching1.activeSelf)    punching1.SetActive(false);

        Task.current.Succeed();
    }

    [Task]
    void setRoute_Random()
    {
        destination = this.transform.position + Random.insideUnitSphere * 5.0f;
        destination.y = this.transform.position.y;

        Task.current.Succeed();
    }

    [Task]
    void setRoute_Player()
    {
        destination = player.position;
        destination.y = this.transform.position.y;

        Task.current.Succeed();
    }

    [Task]
    void moveTo()
    {
        agent.SetDestination(destination);
        bear_anim.SetBool("atk", false);
        if (agent.isStopped)    agent.Resume();
        Task.current.Succeed();
    }

    [Task]
    void PunchReady()
    {
        agent.Stop();
        // punch();
        // Put yo animation script here shrey

        Task.current.Succeed();
    }
    ////////////////////////////// END OF BT STUFF ////////////////////////////////

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;//find player
        agent = GetComponent<NavMeshAgent>();
        bear_anim = GetComponent<Animator>();
    }
    void Start()
    {
        deathVFX.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        deathVFX.SetActive(false);
        deathVFX.transform.position = new Vector3(transform.position.x, transform.position.y + 5, transform.position.z);
        InvokeRepeating("ready", 2, 3);
        punching1.GetComponent<MeshRenderer>().enabled = false;
        punching1.transform.localScale = new Vector3(8f, 8f, 8f);
        punching1.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 direction = player.position - transform.position;//find direction to player
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), 0.3f);//Rotate to player

        distance = Vector3.Distance(player.transform.position, transform.position);//check distance

        if (health <= 0f)
        {
            Vector3 direction1 = deathVFX.transform.position - player.position;//find direction to player
            direction1.y = gameObject.transform.position.y + 2;//find direction to player
            this.GetComponent<PandaBehaviour>().enabled = false;
            //deathVFX.transform.rotation = Quaternion.Slerp(gameObject.transform.rotation, Quaternion.LookRotation(direction1), 0.3f);//Rotate to player
            //deathVFX.transform.rotation = deathVFX.transform.rotation.x + 90;
            deathVFX.SetActive(true);
            
            //smookey.transform.rotation = new Quaternion(smookey.transform.rotation.x, 75, 75, 0);
            Invoke("ahh", 0.3f);
        }
    }

    [Task]
    void punch()
    {
        //gameObject.GetComponent<Rigidbody>().position
        punching1.SetActive(true);
        bear_anim.SetBool("atk", true);
        //punch action (direction and attack range)
        punching1.transform.rotation = transform.rotation;
        punching1.transform.position = new Vector3(transform.position.x, transform.position.y + 1.5f, transform.position.z);
        punching1.transform.Translate(Vector3.forward * 1.75f);
        // Invoke("punchDone", 0.5f);
        Task.current.Succeed();
    }

    [Task]
    void punchDone()
    {
        //gameObject.GetComponent<Rigidbody>().freezeRotation = true;
        punching1.SetActive(false);
        Task.current.Succeed();
    }

    void ahh()
    {
        deathVFX.SetActive(false);
        GameObject smookey = (GameObject)Instantiate(expo, transform.position, transform.rotation);
        smookey.transform.parent = transform;
    }

    void OnCollisionEnter(Collision other)
    {
        // When collides with player punch, destroy
        if (other.gameObject == GameObject.FindGameObjectWithTag("Player"))
        {
            if (playerPunch.activeSelf is true)
            {
                health--;
                GetComponent<Rigidbody>().AddForce(-transform.forward * 10, ForceMode.Impulse);
                StopAllCoroutines();
                StartCoroutine(Flash());
            }
        }
    }

    IEnumerator Flash()
    {
        for (int i = 0; i < 2; ++i)
        {
            GetComponentInChildren<Renderer>().material.color = Color.red;
            yield return new WaitForSeconds(0.1f);
            GetComponentInChildren<Renderer>().material.color = Color.white;
            yield return new WaitForSeconds(0.1f);
            if (health <= 0) GetComponent<Ragdoll>().enabled = true;
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackDistance);
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, idleDistance);
    }

}
