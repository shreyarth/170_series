﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerAttributes : MonoBehaviour
{
    const int INIT_HEALTH = 100;
    // const int INIT_COOL = 100;

    public int health;
    public Slider HealthSlider;
    public int healthRegen = 90;  // Health regeneration amount
    public float healthRegenSec = 1.0f; // Health regeneration speed
    // public int cool;
    // public Slider coolSlider;

    // Ability available?
    // 0: Lost power (Not able to use)
    // 1: Disabled
    // 2: Enabled
    public int flight = 1;
    public Image ico_flight;
    public Image ico_flightDisabled;
    public Image ico_flightLost;
    public int sspeed = 1;
    public Image ico_speed;
    public Image ico_speedLost;
    // public int ssense = 1;
    public int sstrength = 1;
    public Image ico_strength;
    public Image ico_strengthLost;
    // public int laser = 1

    private bool isDead = false;

    // Player damaged related vars
    public Image dImg;  // For screen flashing vfx when hit
    public AudioClip hitSfx;
    private float flashTime = 15f;
    private Color flashColor = new Color(1f, 0f, 0f, 0.2f); // Flash color (tint)
    private bool isHit = false;
    private Vector3 respawnPoint;

    Animation anim;
    public GameObject player;
    Movement movement;

    // Start is called before the first frame update
    void Awake()
    {
        // Regen invoke needs to be in awake since movement gets disabled in the beginning of level
        InvokeRepeating("regeneration", 0f, healthRegenSec);  // Auto-regenerate health
    }
    void Start()
    {
        // anim = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player");
        respawnPoint = player.transform.position;
        movement = player.GetComponent<Movement>();    // Fetch movement script


        // Init Player health
        health = INIT_HEALTH;
        HealthSlider.value = health;
        
        // Init abil Icons
        // Flight
        ico_flight.enabled = false;
        if(flight == 0) ico_flightDisabled.enabled = false;
        else    ico_flightLost.enabled = false;
        // SSpeed
        if(sspeed == 0) ico_speed.enabled = false;
        else    ico_speedLost.enabled = false;
        //SStrength
        if(sstrength == 0) ico_strength.enabled = false;
        else    ico_strengthLost.enabled = false;


        SetRespawn(respawnPoint);  // Store checkpoint
    }

    // Update is called once per frame
    void Update()
    {
        HealthSlider.value = Mathf.MoveTowards(HealthSlider.value, health, 1f);

        if(isHit)
        {
            dImg.color = flashColor;
        }
        else
        {
            dImg.color = Color.Lerp(dImg.color, Color.clear, flashTime * Time.deltaTime);
        }

        isHit = false;

    }

    void regeneration()
    {
        if(health < 100)
        {
            health += healthRegen;
            if (health > 100)   health = INIT_HEALTH;   // In case of overheal, cap the value
        }
    }

    // Call this function when to damage player
    public void damage (int amt)
    {
        isHit = true;

        health -= amt;
        HealthSlider.value = health;

        // If health reachs 0 or lower, kill player
        if(health <= 0 && !isDead)
        {
            isDead = true;
            this.GetComponent<CharacterController>().enabled = false;
            Respawn();
        }
    }

    public void swapFlightIcon ()
    {
        ico_flight.enabled = !ico_flight.enabled;
        ico_flightDisabled.enabled = !ico_flightDisabled.enabled;
    }

    public void movementEnabled (bool en)
    {
        movement.enabled = en;
    }
    
    public void Respawn()
    {
        player.transform.position = respawnPoint;
        health = INIT_HEALTH;
        HealthSlider.value = health;
        isDead = false;
        this.GetComponent<CharacterController>().enabled = true;
    }

    public void SetRespawn(Vector3 lastCheckPoint)
    {
        respawnPoint = lastCheckPoint;
    }
}