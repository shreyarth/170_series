﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMovement : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        //hit the object to make it fly, please give a tag for objects first
        if (collision.gameObject.tag == "object")
        {
            Vector3 direction = transform.position - collision.transform.position;
            gameObject.GetComponent<Rigidbody>().AddForce(direction.x * 1000, 0, direction.z * 1000);
        }
    }
}
