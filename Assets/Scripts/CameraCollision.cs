﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCollision : MonoBehaviour
{
	public float minDistance = 1.0f;
	public float maxDistance = 4.0f;
	public float lerp = 10.0f;
	Vector3 direction;
	public Vector3 directionAdj;
	public float distance;

    // Start is called before the first frame update
    void Awake()
    {
        direction = transform.localPosition.normalized;
        distance = transform.localPosition.magnitude;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 newCameraPos = transform.GetChild(0).TransformPoint(direction * maxDistance);
        RaycastHit hit;

        if (Physics.Linecast(transform.GetChild(0).position, newCameraPos, out hit))
        {
        	distance = Mathf.Clamp ((hit.distance * 0.9f), minDistance, maxDistance);
        }
        else
        {
        	distance = maxDistance;
        }

       transform.localPosition = Vector3.Lerp(transform.localPosition, direction * distance, Time.deltaTime * lerp);
    }
}
