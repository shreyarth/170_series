﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class temp : MonoBehaviour
{
    GameObject temper;
    // Start is called before the first frame update
    void Start()
    {
        temper = GameObject.Find("lt");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            temper.SetActive(false);
        }
        else
        {
            temper.SetActive(true);
        }
    }
}
