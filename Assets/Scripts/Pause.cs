﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour
{
    public bool IsGamePaused;
    public GameObject menuLayer;
    public GameObject camera;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            camera.GetComponent<CameraMovement>().enabled = false;
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = true;
            PauseGame();
            menuLayer.SetActive(true);
        }
    }

    void PauseGame()
    {
        IsGamePaused = true;
        Time.timeScale = 0;
    }

    public bool isPaused()
    {
        return IsGamePaused;
    }

    public void setPaused(bool newset)
    {
        IsGamePaused = newset;
    }
}