﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Punch : MonoBehaviour
{
    #region Punch
    [Header("Punch Object")]
    [SerializeField]
    private GameObject punch;
    [SerializeField]
    private float punchTimer = 1.0f;
    bool pun = false;
    #endregion

    [SerializeField]
    private GameObject powObj;
    [SerializeField]
    private AudioSource source;
    [SerializeField]
    private AudioClip whoosh;
    private Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        punch.SetActive(false);
        powObj.SetActive(false);
        anim = GetComponent<Animator>();
        source = GetComponent<AudioSource>();
        powObj.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
    }


    // Update is called once per frame
    void Update()
    {
        //Spawn a box in front of player at punch height
        if (Input.GetButtonDown("Punch"))
        {
            //anim.SetTrigger("punch");
            anim.SetBool("punch", true);
            punch.SetActive(true);
            pun = true;
            source.PlayOneShot(whoosh, 1f);
        }

        //Assign punch timer
        if (pun == true)
        {
            punchTimer -= Time.deltaTime;
            powObj.transform.localScale += new Vector3(0.06f, 0.06f, 0.06f);
        }
        //if timer reaches 0, punch vanishes
        if (Input.GetButtonUp("Punch") || punchTimer <= 0)
        {
            // Debug.Log("dwads");
            anim.SetBool("punch", false);
            punch.SetActive(false);
            punchTimer = 1.0f;
            pun = false;
            powObj.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            StopAllCoroutines();
            StartCoroutine("Pow");
        }
        if (collision.gameObject.CompareTag("Boss"))
        {
            StopAllCoroutines();
            StartCoroutine("Pow");
        }
    }

    IEnumerator Pow()
    {
        powObj.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        powObj.SetActive(false);
    }
}
