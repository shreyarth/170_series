﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoints : MonoBehaviour
{
    public PlayerAttributes respawnManager;

    // Start is called before the first frame update
    void Awake()
    {
        respawnManager = FindObjectOfType<PlayerAttributes>();
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            respawnManager.SetRespawn(transform.position);
            gameObject.GetComponent<Renderer>().enabled = false;
        }
    }
}
