﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class warningText : MonoBehaviour
{
	public GameObject textbox;
	GameObject player;
    public string msg_string;
	textbox tb;

	void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");  // Find player object
        tb = textbox.GetComponent<textbox>();
    }

    void OnTriggerEnter(Collider other)
    {
    	if(other.gameObject.CompareTag("Player"))
    	{
    		// Line that calls textbox (string 'msg', float 'sec', Vector2 'boxpos')
    		tb.callCaption(msg_string, Mathf.Infinity, new Vector2(0, 0));
    		textbox.SetActive(true);
    	}
    }

    void OnTriggerExit(Collider other)
    {
    	if(other.gameObject.CompareTag("Player"))
    	{
    		textbox.SetActive(false);
    	}
    }
}