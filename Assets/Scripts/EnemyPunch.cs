﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPunch : MonoBehaviour
{
    // Start is called before the first frame update
    
    GameObject player;
    PlayerAttributes pa;
    public int damage = 10;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");//find player
        pa = player.GetComponent<PlayerAttributes>();
    }
    
    void Start()
    {
        gameObject.GetComponent<Collider>().enabled = true;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.gameObject.tag == "Player")
        {
            pa.damage(damage);
        }

        Vector3 direction = player.transform.position - transform.position;//find direction to player
        gameObject.GetComponent<Rigidbody>().AddForce(direction.x * 100, 0, direction.z * 100);
    }
}
