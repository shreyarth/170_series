﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowScript : MonoBehaviour
{
    public GameObject powObj;
    // Start is called before the first frame update
    void Awake()
    {
        powObj.SetActive(false);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            StopAllCoroutines();
            StartCoroutine("Pow");
        }
    }
    IEnumerator Pow()
    {
        powObj.SetActive(true);
        yield return new WaitForSeconds(0.2f);
        powObj.SetActive(false);
    }
}
