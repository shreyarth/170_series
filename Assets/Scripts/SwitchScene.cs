﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwitchScene : MonoBehaviour
{
	public string SceneName;
    HostageTracker Tracker;

    public GameObject textbox;
    textbox tb;

    void Awake()
    {
        Tracker = GameObject.FindGameObjectWithTag("Player").GetComponent<HostageTracker>();
        tb = textbox.GetComponent<textbox>();
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player") && !Tracker.checkValid())
        {
            tb.callCaption("Need to save hostages!", 4.5f, new Vector2(0f, -150f));
            textbox.SetActive(true);
        }
        if(other.CompareTag("Player") && Tracker.checkValid())
        {
            SceneManager.LoadScene(SceneName);
        }
    }
}