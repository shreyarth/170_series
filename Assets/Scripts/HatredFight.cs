﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AI;

public class HatredFight : MonoBehaviour
{
    float distance;
    GameObject ball;
    public GameObject punching;
    GameObject burst1;
    GameObject burst2;
    GameObject burst3;
    GameObject hero;
    int ranNumber;
    int ranNumberH;
    float ranNumberX;
    float ranNumberXH;
    float ranNumberZ;
    float ranNumberZH;
    Vector3 randomDirection;
    Vector3 dest;
    float timer=10f;
    public float health = 5;

    Transform player;
    GameObject pPunch;
    LineRenderer pLaser;
    PlayerAttributes pa;
    Movement dunno;
    public Animator hAnim;

    public GameObject smooke;
    
    NavMeshAgent agent;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;//find player
        pa = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerAttributes>();
        pLaser = player.GetChild(2).gameObject.GetComponent<LineRenderer>();
        pPunch = player.GetChild(3).gameObject;
        hAnim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
    }

    void Start()
    {
        hero = GameObject.FindGameObjectWithTag("Player");
        dunno = hero.GetComponent<Movement>();
        // Debug.Log(dunno);
        //move
        //shooting attack
        InvokeRepeating("shoot", 1, 2);
        //flashing attack
        InvokeRepeating("flash", 1, 4);
        //burst attack
        InvokeRepeating("burst", 1, 2);
        //punch from flashing attack
        //punching = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        punching.transform.localScale = new Vector3(10, 10, 10);
        punching.SetActive(false);

        //inital random destination
        ranNumberH = Random.Range(0, 2);
        if (ranNumberH == 0)
        {
            ranNumberXH = -15;
        }
        else
        {
            ranNumberXH = 15;
        }

        ranNumberH = Random.Range(0, 2);
        if (ranNumber == 0)
        {
            ranNumberZH = -15;
        }
        else
        {
            ranNumberZH = 15;
        }
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 direction = player.position - transform.position;//find direction to player
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), 0.3f);//Rotate to player

        distance = Vector3.Distance(player.transform.position, transform.position);//check distance


        //movement 
        timer -= Time.deltaTime;
        if(timer<=2 && timer > 1.5f)
        {
            //set destination
            dest = new Vector3(player.position.x + ranNumberXH, player.position.y+0.5f, player.position.z + ranNumberZH);
            //check whether it reaches destination
            // if(transform.position != dest)
            // {
            //     transform.position = Vector3.MoveTowards(transform.position, dest, 0.8f);
            // }
            // //stop there if reaching there
            // else
            // {
            //     transform.position += Vector3.zero;
            // }
            agent.SetDestination(dest);
        }

        //reset the random destination
        if(timer<=0)
        {
            ranNumberH = Random.Range(0, 2);
            if (ranNumberH == 0)
            {
                ranNumberXH = -15;
            }
            else
            {
                ranNumberXH = 15;
            }

            ranNumberH = Random.Range(0, 2);
            if (ranNumber == 0)
            {
                ranNumberZH = -15;
            }
            else
            {
                ranNumberZH = 15;
            }
            timer = 10.0f;
        }
        // hatred death, load cutscene
        if (health <= 0)
        {
            SceneManager.LoadScene("Cutscene5");
        }
    }

    //range attack
    void shoot()
    {
        //check distance
        if (distance > 20)
        {
            hAnim.SetBool("isCharge", true);
            ball= GameObject.CreatePrimitive(PrimitiveType.Sphere);
            ball.GetComponent<Collider>().isTrigger = true;
            ball.transform.position = new Vector3(transform.position.x+1, transform.position.y+1, transform.position.z+1);
            ball.gameObject.GetComponent<Renderer>().material = Resources.Load("tempbullets") as Material;//material
            ball.AddComponent<HatBullet>();
            ball.AddComponent<EnemyPunch>();

            GameObject smookey = (GameObject)Instantiate(smooke, ball.transform.position, ball.transform.rotation);
            smookey.transform.parent = ball.transform;
            smookey.transform.rotation = new Quaternion(smookey.transform.rotation.x, 75, 75, 0);
        }
        else
        {
            hAnim.SetBool("isCharge", false);
        }
    }

    //melee attack
    //disappear and flashing move around player
    void flash()
    {

        //Check distance
        if (distance <= 10)
        {
            //random position
            ranNumber = Random.Range(0, 2);
            if (ranNumber==0)
            {
                ranNumberX = -1;
            }
            else
            {
                ranNumberX = 1;
            }

            ranNumber = Random.Range(0, 2);
            if (ranNumber == 0)
            {
                ranNumberZ = -1;
            }
            else
            {
                ranNumberZ = 1;
            }

            //disappear
            gameObject.SetActive(false);
            Invoke("appear", 0.5f);
        }
    }

    //appear again
    void appear()
    {
        gameObject.SetActive(true);
        gameObject.transform.position = new Vector3(player.position.x + ranNumberX, player.position.y, player.position.z + ranNumberZ);
        hAnim.SetBool("isPunch", true);
        Invoke("punch", 0.5f);
    }

    //punch
    void punch()
    {
        punching.SetActive(true);
        //punch action (direction and attack range)
        punching.transform.rotation = transform.rotation;
        punching.transform.position = new Vector3(transform.position.x, transform.position.y+1, transform.position.z);
        punching.transform.Translate(Vector3.forward * 1.75f);
        punching.GetComponent<MeshRenderer>().enabled = false;
        Invoke("punchDone", 0.5f);
    }

    //punch done
    void punchDone()
    {
        hAnim.SetBool("isPunch", false);
        punching.SetActive(false);
    }

    //burst attack
    void burst()
    {
        if (distance <= 20 && distance > 10)
        {
            hAnim.SetBool("isCharge", true);
            //3 HatBullets (it will be better to set them in an array)
            burst1 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            burst1.GetComponent<Collider>().isTrigger = true;
            burst1.gameObject.GetComponent<Renderer>().material = Resources.Load("tempbullets") as Material;
            burst1.transform.position = new Vector3(transform.position.x, transform.position.y + 4, transform.position.z);

            //different shooting time
            Invoke("burstShoot1", 1);

            burst2 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            burst2.GetComponent<Collider>().isTrigger = true;
            burst2.transform.position = new Vector3(transform.position.x-3, transform.position.y + 2, transform.position.z+3);
            burst2.gameObject.GetComponent<Renderer>().material = Resources.Load("tempbullets") as Material;

            //different shooting time
            Invoke("burstShoot2", 1.5f);

            burst3 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            burst3.GetComponent<Collider>().isTrigger = true;
            burst3.transform.position = new Vector3(transform.position.x+3, transform.position.y + 2, transform.position.z-3);
            burst3.gameObject.GetComponent<Renderer>().material = Resources.Load("tempbullets") as Material;

            //different shooting time
            Invoke("burstShoot3", 2f);
        }
        else
        {
            hAnim.SetBool("isCharge", false);
        }
    }

    //add components to burst HatBullets (it will be better to set them in an array)
    void burstShoot1()
    {
        burst1.AddComponent<EnemyPunch>();
        burst1.AddComponent<HatBullet>();
        GameObject smookey = (GameObject)Instantiate(smooke, burst1.transform.position, burst1.transform.rotation);
        smookey.transform.parent = burst1.transform;
        smookey.transform.rotation = new Quaternion(smookey.transform.rotation.x, 75, 75, 0);
    }

    void burstShoot2()
    {
        burst2.AddComponent<EnemyPunch>();
        burst2.AddComponent<HatBullet>();
        GameObject smookey = (GameObject)Instantiate(smooke, burst2.transform.position, burst2.transform.rotation);
        smookey.transform.parent = burst2.transform;
        smookey.transform.rotation = new Quaternion(smookey.transform.rotation.x, 75, 75, 0);
    }

    void burstShoot3()
    {
        burst3.AddComponent<EnemyPunch>();
        burst3.AddComponent<HatBullet>();
        GameObject smookey = (GameObject)Instantiate(smooke, burst3.transform.position, burst3.transform.rotation);
        smookey.transform.parent = burst3.transform;
        smookey.transform.rotation = new Quaternion(smookey.transform.rotation.x, 75, 75, 0);
    }

    /*void move()
    {
       //random position
     
    }

    void Go()
    {
        //gameObject.SetActive(true);
    }*/

    void OnCollisionEnter(Collision other)
    {
        // When collides with player punch, destroy
        // Debug.Log(punch);
        if (other.gameObject == GameObject.FindGameObjectWithTag("Player"))
        {
            if (pPunch.activeSelf is true)
            {
                health--;
                StopCoroutine(Dmg());
                StartCoroutine(Dmg());
                Debug.Log(health);
            }

            
        }
    }

    IEnumerator Dmg()
    {
        for (int i = 0; i < 2; ++i)
        {

            gameObject.transform.GetChild(1).GetComponentInChildren<SkinnedMeshRenderer>().material.color = Color.green;
            gameObject.transform.GetChild(2).GetComponentInChildren<SkinnedMeshRenderer>().material.color = Color.green;
            yield return new WaitForSeconds(0.1f);
            gameObject.transform.GetChild(1).GetComponentInChildren<SkinnedMeshRenderer>().material.color = Color.yellow;
            gameObject.transform.GetChild(2).GetComponentInChildren<SkinnedMeshRenderer>().material.color = Color.yellow;
            yield return new WaitForSeconds(0.1f);
        }
    }
}
