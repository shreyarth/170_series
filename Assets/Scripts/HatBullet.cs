﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HatBullet : MonoBehaviour
{
    Transform player;
    float speed = 0.5f;
    Vector3 dest;
    float timer = 3.0f;

    PlayerAttributes pa;
    public GameObject smooke;
    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;//find player
        pa = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerAttributes>();

    }
    void Start()
    {
        //Hatred_Orb_VFX
        dest = new Vector3(player.position.x, player.position.y, player.position.z);// set the shooting destination 

        //Add smoke to bullets; try to add it as a child, but nothing happens
        GameObject smoke = (GameObject)Resources.Load("VFX/Hatred_Orb/Hatred_Orb_VFX");
        smoke.transform.parent = gameObject.transform;

        Instantiate(smooke, transform.position,transform.rotation);
        smooke.transform.parent = transform;
        //GameObject GO = Instantiate(smoke) as GameObject;


        //GO.transform.parent = transform;
        //GO.transform.position = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
        timer -= Time.deltaTime;

        //make the bullet bigger
        if (timer <= 3 && timer > 2)
        {
            transform.localScale *= 1.01f;
        }

        //shoot
        if (timer <= 2)
        {
            transform.position = Vector3.MoveTowards(transform.position, dest, speed);
        }

        //destroy
        if (timer <= 0)
        {
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter(Collision other)
    {
        //damage player
        if (other.gameObject.tag == "Player")
        {
            pa.damage(10);
            Destroy(gameObject);
        }
    }
}
