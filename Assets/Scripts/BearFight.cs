﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class BearFight : MonoBehaviour
{
    Transform player;
    float distance;
    Vector3 destination;
    GameObject traps;
    public GameObject charging;
    public GameObject punching1;
    public GameObject punching2;
    public GameObject punching3;
    float timer = 8f;
    float ranX = 0;
    float ranZ = 0;
    int ran = 0;
    public int health = 10;
    NavMeshAgent agent;
    public GameObject punch;
    public string SceneName;
    private Animator hat_anim;
    public float hp;

    // Start is called before the first frame update
    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;//find player
        agent = GetComponent<NavMeshAgent>();
        hat_anim = GetComponent<Animator>();
    }

    void Start()
    {

        InvokeRepeating("flash", 2, 4);
        //InvokeRepeating("punch1",2, 3);
        //set charging attackinbg ball
        charging.GetComponent<MeshRenderer>().enabled = false;
        charging.transform.localScale = new Vector3(10, 10, 10);
        charging.SetActive(false);

        //set punch ball
        punching1.transform.localScale = new Vector3(10, 10, 10);
        punching1.GetComponent<MeshRenderer>().enabled = false;
        //punching1.AddComponent<EnemyPunch>();
        punching1.SetActive(false);

       // punching2 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        punching2.transform.localScale = new Vector3(10, 10, 10);
        punching2.GetComponent<MeshRenderer>().enabled = false;
       // punching2.AddComponent<EnemyPunch>();
        punching2.SetActive(false);

        //punching3 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        punching3.transform.localScale = new Vector3(10, 10, 10);
        punching3.GetComponent<MeshRenderer>().enabled = false;
        //punching3.AddComponent<EnemyPunch>();
        punching3.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        // Check health
        if(health <= 0)
        {
            // GlobalVariables.UpdateCheckpoint(1);
            // GlobalVariables.SaveCheckpointData();
            SceneManager.LoadScene(SceneName);
        }
        Vector3 direction = player.position - transform.position;//find direction to player
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), 0.3f);//Rotate to player

        distance = Vector3.Distance(player.transform.position, transform.position);//check distance


        //charging timer
        timer -= Time.deltaTime;
        //set destination
        if(timer <=2 && timer >1.9f)
        {
            destination = player.position;
        }
        //charging
        if (timer <= 1.9f && timer > 1f)
        {
            charging.transform.rotation = transform.rotation;
            charging.transform.position = new Vector3(transform.position.x, transform.position.y + 1, transform.position.z);
            charging.transform.Translate(Vector3.forward);
            charging.SetActive(true);  
            //set destination
            //check whether it reaches destination
            if (transform.position != destination)
            {
                // transform.position = Vector3.MoveTowards(transform.position, destination, 1.5f);
                agent.SetDestination(destination);
            }
            //stop there if reaching there
            else
            {
                transform.position += Vector3.zero;
                //charging.SetActive(false);
            }

        }
        else
        {
            //transform.position += Vector3.zero;
            charging.SetActive(false);
        }

        //reset charge timer
        if (timer <= 0)
        {
            timer = 10.0f;
            charging.SetActive(false);
            //poop a trap
           // Invoke("trap", 0.1f);
        }
    }
    /*
    void trap()
    {
        //create a trap
        if (distance<10)
        {
            traps = GameObject.CreatePrimitive(PrimitiveType.Cube);
            traps.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            traps.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
            traps.AddComponent<Bosstrap>();
        }
    }
    */
    void flash()
    {
        gameObject.SetActive(false);
        //flash jump
        if (distance >= 5)
        {
            Invoke("appear", 0.5f);
        }
        if (distance < 5)
        {
            Invoke("punchch", 0.5f);
        }
    }

    void appear()
    {
        //rush to destination
        gameObject.SetActive(true);
        gameObject.transform.position = new Vector3(gameObject.transform.position.x, player.position.y+20, gameObject.transform.position.z);
        Invoke("charge", 0.5f);
    }

    void charge()
    {
        //make the jump and charge happen soon
        timer = 3f;
    }

    //set 3 punching combo
    void punchch()
    {
        ran = Random.Range(0, 2);
        if (ran == 0)
        {
            ranX= -1.8f;
        }
        else
        {
            ranX = 1.8f;
        }

        ran = Random.Range(0, 2);
        if (ran == 0)
        {
            ranZ = -1.8f;
        }
        else
        {
            ranZ = 1.8f;
        }
        gameObject.transform.position = new Vector3(player.position.x + ranX, player.position.y+0.5f, player.position.z + ranZ);
        gameObject.SetActive(true);
        
        Invoke("punch1", 0.5f);

    }
    void punch1()
    {
        punching1.SetActive(true);
        //punch action (direction and attack range)
        punching1.transform.rotation = transform.rotation;
        punching1.transform.position = new Vector3(transform.position.x, transform.position.y + 1.5f, transform.position.z);
        punching1.transform.Translate(Vector3.forward * 1.5f);
        Invoke("punchDone1", 0.2f);
        timer = 5f;
        hat_anim.SetBool("atk", true);
    }

    //punch done
    void punchDone1()
    {
        punching1.SetActive(false);
        Invoke("punch2", 0.2f);
    }

    void punch2()
    {
        punching2.SetActive(true);
        //punch action (direction and attack range)
        punching2.transform.rotation = transform.rotation;
        punching2.transform.position = new Vector3(transform.position.x, transform.position.y + 1f, transform.position.z);
        punching2.transform.Translate(Vector3.forward * 1.5f);
        Invoke("punchDone2", 0.2f);
        timer = 5f;
    }

    //punch done
    void punchDone2()
    {
        punching2.SetActive(false);
        Invoke("punch3", 0.5f);
    }

    void punch3()
    {
        punching3.SetActive(true);
        //punch action (direction and attack range)
        punching3.transform.rotation = transform.rotation;
        punching3.transform.position = new Vector3(transform.position.x, transform.position.y + 1f, transform.position.z);
        punching3.transform.Translate(Vector3.forward * 1.75f);
        Invoke("punchDone3", 0.5f);
        timer = 5f;
    }

    //punch done
    void punchDone3()
    {
        punching3.SetActive(false);
        hat_anim.SetBool("atk", false);
    }

    private void OnCollisionEnter(Collision collision)
    {
        //when the hatred hit the ground
        if (collision.transform.tag == "parking")
        {
            //explosion force
            if (distance < 5)
            {
                Debug.Log("222222");
                //add force
                float ForceN = 2000000 / (new Vector3(player.position.x - transform.position.x, player.position.y, player.position.z - transform.position.z)).magnitude;
                player.gameObject.GetComponent<Rigidbody>().AddForce((player.transform.position - transform.position).normalized * ForceN);
            }
        }

        if (collision.gameObject == GameObject.FindGameObjectWithTag("Player"))
        {
            if (punch.activeSelf is true)
            {
                health--;
                GetComponent<Rigidbody>().AddForce(-transform.forward * 10, ForceMode.Impulse);
            }
        }
    }
}
