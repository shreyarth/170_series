﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ragdoll : MonoBehaviour
{
    public int hp;
    // Start is called before the first frame update
    void Start()
    {
        SetRigidbodyState(true);
        SetColliderState(false);
    }

    // Update is called once per frame
    void Update()
    {
        hp = GetComponent<MeleeEnemies>().health;
        if (hp <= 0)
        {
            Invoke("Die", 0.5f);
        }
    }

    void SetRigidbodyState(bool state)
    {
        Rigidbody[] bodies = GetComponentsInChildren<Rigidbody>();
        foreach (Rigidbody rb in bodies)
        {
            rb.isKinematic = state;
        }

        GetComponent<Rigidbody>().isKinematic = !state;
    }

    void SetColliderState(bool state)
    {
        Collider[] colliders = GetComponentsInChildren<Collider>();     
        foreach (Collider cl in colliders)
        {
            cl.enabled = state;
        }

        GetComponent<Collider>().enabled = !state;
    }
    void Die()
    {
        SetRigidbodyState(false);
        SetColliderState(true);
       // GetComponent<Animator>().enabled = false;
        GetComponent<MeleeEnemies>().enabled = false;
        // GetComponent<NavMeshAgent>().enabled = false;
        Destroy(gameObject, 5);
    }
}
