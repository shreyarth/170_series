﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

using Panda;

public class EnemyRabbit : MonoBehaviour
{
    private float timer = 1.0f;
    private float distance;
    private Vector3 dir;
    [SerializeField]public int bHealth;
    
    public GameObject bulletObj;
    List<GameObject> bullets;
    public float rangeTimer = 2.0f;
    public int ammo = 5;    // Pool amount
    [Range(0.0f, 20.0f)]
    public float shootDistance = 10.0f;
    [Range(0.0f, 10.0f)]
    public float safeDistance = 5.0f;

    Transform player;
    GameObject punch;
    Vector3 randomDirection;
    Vector3 destination;
    Movement movement;

    public float powval;

    private AudioSource miss;
    public AudioClip missile;
    Animator b_anim;
    NavMeshAgent agent;


    ////////////////////////// Behavior tree objs & methods ////////////////////////////
    bool _isIdle = true;
    [Task]
    bool isIdle
    {
        get
        {
            return distance > shootDistance;
        }
    }

    bool _isSafe = true;
    [Task]
    bool isSafe
    {
        get
        {
            return distance > safeDistance;
        }
    }

    [Task]
    void setRoute_Random()
    {
        destination = this.transform.position + Random.insideUnitSphere * 5.0f;
        destination.y = this.transform.position.y;
        
        while (Vector3.Distance(player.transform.position, destination) < safeDistance)
        {
            destination = this.transform.position + Random.insideUnitSphere * 5.0f;
            destination.y = this.transform.position.y;
        }
        

        Task.current.Succeed();
    }

    [Task]
    void moveTo()
    {
        agent.SetDestination(destination);
        b_anim.SetBool("atk", false);
        agent.Resume();
        Task.current.Succeed();
    }

    [Task]
    void Fire()
    {
        agent.Stop();
        fire();
        b_anim.SetBool("atk", true);
        // Debug.Log(b_anim.GetBool("atk"));
        Task.current.Succeed();
    }
    ////////////////////////////// END OF BT STUFF ////////////////////////////////

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;  // Find player
        punch = player.GetChild(3).gameObject;
        movement = player.GetComponent<Movement>();
        agent = GetComponent<NavMeshAgent>();
        powval = movement.powvalue;

        miss = GetComponent<AudioSource>();
        dir.x = player.position.x;  // Save last seen player position
        dir.y = player.position.y + 1;  // Save last seen player position
        dir.z = player.position.z;  // Save last seen player position
        bulletObj.SetActive(false);
    }

    void Start()
    {
        b_anim = GetComponent<Animator>();
        randomDirection = new Vector3(Random.Range(-359, 359), 0, Random.Range(-359, 359));
        // bulletObj = GameObject.CreatePrimitive(PrimitiveType.Sphere);   // Temp bullet obj
        // bulletObj.GetComponent<Renderer>().material.color = Color.black;
        
        // bulletObj.AddComponent<Bullet>();
        // bulletObj.GetComponent<Collider>().isTrigger = true;
        // bulletObj.transform.localScale = new Vector3 (0.2f, 0.2f, 0.2f);
        /*
        bullets = new List<GameObject>();   // Create pool
        // Init obj pool
        for (int i = 0; i < ammo; i++)
        {
            GameObject obj = (GameObject)Instantiate(bulletObj);
            obj.SetActive(false);
            bullets.Add(obj);
        }
       */
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(bulletObj.activeSelf);
        distance = Vector3.Distance(player.transform.position, transform.position);
        if(shootDistance >= distance)
        {
            Vector3 direction = player.position - transform.position;   // Find direction to player
            direction.y = 0;    // Init Y axis to 0 to prevent floating enemies
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), 0.3f);    // Rotate to player
        }
        if(bHealth <= 0f)
        {
            b_anim.enabled = false;
            // Disable behavior tree when bunny health hits 0
            this.GetComponent<PandaBehaviour>().enabled = false;
            // destination = Vector3.zero;
            agent.Stop();
            Debug.Log("I am inevitable");
        }
    }

    void OnCollisionEnter(Collision other)
    {
        // When collides with player punch, destroy
        if (other.gameObject == GameObject.FindGameObjectWithTag("Player"))
        {
            if (punch.activeSelf is true)
            {
                bHealth--;
                GetComponent<Rigidbody>().AddForce(-transform.forward * 10, ForceMode.Impulse);
                StopAllCoroutines();
                StartCoroutine(Flash());
            }
        }
    }

    // Search inactive object and fire (If none available, it won't fire)
    public void fire ()
    {
        /*
        for (int i = 0; i < bullets.Count; i++)
        {
            if(!bullets[i].activeInHierarchy)
            {
                bullets[i].transform.position = transform.position + new Vector3(0f, 2f, 1f);
                // bullets[i].transform.rotation = Quaternion.LookRotation(player.position);
                // Debug.Log("Pre: " + bullets[i].transform.rotation);
                bullets[i].transform.rotation = Quaternion.LookRotation(player.position - transform.position);
                // Debug.Log("Post: " + bullets[i].transform.rotation);
                bullets[i].SetActive(true);
                miss.Play();
                break;
            }
        }
        */
        bulletObj.SetActive(true);
    }

    // To set new obj pool amount outside of script, call this function
    public void setBulletAmount(int newAmt)
    {
        ammo = newAmt;
    }

    IEnumerator Flash()
    {
        for(int i = 0; i < 2; ++i)
        {
            GetComponentInChildren<Renderer>().material.color = Color.red;
            yield return new WaitForSeconds(0.1f);
            GetComponentInChildren<Renderer>().material.color = Color.white;
            yield return new WaitForSeconds(0.1f);
            if (bHealth <= 0) GetComponent<Ragdoll>().enabled=true;
        }
    }

    public bool checkDistance()
    {
        return distance > shootDistance;
    }

    public float getRange()
    {
        return shootDistance;
    }

    public void SetDestination(Vector3 newDestination)
    {
        destination = newDestination;
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, shootDistance);
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, safeDistance);
    }
}