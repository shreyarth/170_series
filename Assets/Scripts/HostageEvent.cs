using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HostageEvent : MonoBehaviour
{
    public float timer = 34.0f;
    float yellingTimer = 10.0f;
    private float distance;
    private bool startCountdown = false;
    [Range(0.0f, 10.0f)]
    public float rescueRadius = 10.0f;
    // [Range(0.0f, 10.0f)]
    // public float enemyRadius = 10.0f;
    HostageTracker Tracker;

    Camera camera1;
    Camera camera2;
    // GameObject hostage;
    GameObject terrorist;
    GameObject player;

    public GameObject help;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");  // Find player pos
        // Debug.Log(player);
        Tracker = player.GetComponent<HostageTracker>();
    }

    void Start()
    {
        help.transform.localScale = new Vector3(3, 3, 3);
        help.SetActive(false);
        help.transform.position = new Vector3(transform.position.x, transform.position.y + 5, transform.position.z);
        InvokeRepeating("bangju", 2, 5);
        //StartCoroutine("Halp");
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 direction = help.transform.position-player.transform.position;//find direction to player
        help.transform.rotation = Quaternion.Slerp(help.transform.rotation, Quaternion.LookRotation(direction), 0.3f);//Rotate to player
        float distance = Vector3.Distance(player.transform.position, transform.position);
        if(distance < rescueRadius)
        {
            Tracker.setValueTrue(this.name);
            Destroy(this.gameObject);
        }

        if(startCountdown)
        {
            countdown();
        }
        else
        {
            // Calculate distance between player and hostage until countdown starts
            distance = Vector3.Distance(player.transform.position, transform.position);
            // If ditance threshold met, start countdown
            if (distance < 100f)
            {
                Debug.Log("cdown init");
                startCountdown = true;
            }
        }
    }

    void countdown()
    {
        timer -= Time.deltaTime;
        // Do we need this cam script?
        if (timer <= 4f)
        {
            // Debug.Log("aaa");
            // camera1.enabled = false;
            // camera2.enabled = true;
        }
        else
        {
            if (timer <= 0f) // Hostage dies
            {
                //float step = Time.deltaTime;
                //terrorist.GetComponent<Rigidbody>().velocity = new Vector3(0, 1, 0);
                Destroy(this.gameObject);
            }
        }
    }

    void OnCollisionEnter(Collision other)
    {
        // Debug.Log("Colliding with: " + other.gameObject);
        // When collides with player, destroy
        if (other.gameObject == player)
        {
            Destroy(this.gameObject);
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(transform.position, rescueRadius);
    }
    
    void bangju()
    {
        help.SetActive(true);
        // Debug.Log("wadsadawdwa");
        Invoke("bob", 2f);
    }

    void bob()
    {
        help.SetActive(false);
    }
    
}