﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    private Transform player;
    private Transform mainCam;
    private Transform playerGroundPosition;
    public float rotateSpeed;
    public Vector3 offset;
    public Transform pivot;
    public LayerMask wallLayer;
    // Use this for initialization

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;  // Find player
        mainCam = Camera.main.transform;
    }

    void Start()
    {
        offset = player.position - transform.position + new Vector3(0f, 7f, -1f);
        pivot.transform.position = player.position + offset;
        pivot.transform.parent = player;
        //pivot.transform.parent = null;
        // Locking the cursor
        Cursor.lockState = CursorLockMode.Locked; // Uncomment when release
        Cursor.visible = false;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        //pivot.transform.position = player.position;
        //Rotation with mouse
        float horizontal = Input.GetAxis("Mouse X") * rotateSpeed;
        player.Rotate(0, horizontal, 0);
        float vertical = Input.GetAxis("Mouse Y") * rotateSpeed;

        if (player.GetComponent<Rigidbody>().useGravity == true)
        {
            pivot.Rotate(-vertical, 0, 0);
            //Rotation lock
            if(pivot.rotation.eulerAngles.x > 10f && pivot.rotation.eulerAngles.x < 180f)
            {
                pivot.rotation = Quaternion.Euler(10f, 0f, 0f);
            }
            if (pivot.rotation.eulerAngles.x > 180f && pivot.rotation.eulerAngles.x < 307f)
            {
                pivot.rotation = Quaternion.Euler(307f, 0f, 0f);
            }
            float rotateXAngle = pivot.eulerAngles.x;
            float rotateYAngle = player.eulerAngles.y;
            Quaternion rotate = Quaternion.Euler(rotateXAngle, rotateYAngle, 0);
            transform.position = player.position - (rotate * -offset);
            transform.LookAt(player.position + new Vector3(0f, 2.5f, 0f));
        }
        else
        {
            player.Rotate(-vertical, 0, 0);
            float rotateXAngle = player.eulerAngles.x;
            float rotateYAngle = player.eulerAngles.y;
            Quaternion rotate = Quaternion.Euler(rotateXAngle, rotateYAngle, 0);
            transform.position = player.position - (rotate * -offset);
            transform.LookAt(player.position);
        }


        camAdjustment();
    }

    void camAdjustment ()
    {
        RaycastHit hit;
        if(Physics.Linecast(player.position + new Vector3(0f, 2.5f, 0f), mainCam.position, out hit, wallLayer))
        {
            mainCam.localPosition = new Vector3 (hit.point.x + 0.4f, transform.localPosition.y, hit.point.z + 0.5f);
            mainCam.LookAt(player.position + new Vector3(0f, 2.5f, 0f));
        }
    }
}
