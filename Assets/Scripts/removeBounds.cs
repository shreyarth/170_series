﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class removeBounds : MonoBehaviour
{
	public GameObject boundGroup;
	bool triggered = false;

    void OnTriggerEnter (Collider other)
    {
        if(other.gameObject.CompareTag("Player") && !triggered)
        {
            int count = boundGroup.transform.childCount;
			for(int i = 0; i < count; ++i)
			{
				boundGroup.transform.GetChild(i).gameObject.SetActive(false);
			}

			triggered = true;	// Trigger only once
        }
    }
}
