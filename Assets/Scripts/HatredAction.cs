﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HatredAction : MonoBehaviour
{
    public int meleeDmg = 10;
    public int rangeDmg = 10;

    private float meleeTimer = 3.0f;
    private float rangeTimer = 2.0f;
    private float checkTimer = 1.0f;
    private float startTimer = 7.0f; 
    private float distance;
    private Vector3 direction;
    private bool isPlay = false;

    GameObject player;
    Transform playerPos;
    PlayerAttributes pa;

    public Camera cameraMain;
    public Camera camerahat;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");  // Find player object
        playerPos = GameObject.FindGameObjectWithTag("Player").transform;  // Store player transform
        pa = player.GetComponent<PlayerAttributes>();
    }

    // Start is called before the first frame update
    void Start()
    {
        //randomValue = Random.Range(1, 3);
        GetComponent<Rigidbody>().useGravity = false;
        // Init game with boss camera in start
        cameraMain.enabled = false;
        camerahat.enabled = true;
        pa.movementEnabled(false);     // Disable player movement script when camera is switched
    }

    // Update is called once per frame
    void Update()
    {
        /////////////////////// Beginning event ///////////////////////
        if (startTimer > 0)
        {
            startTimer -= Time.deltaTime;
            // Move to designated position
            if (startTimer <= 3)
            {
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(20, 50, 75), 5);
            }
        }
        //////////////////// End of beginning event ////////////////////
        else
        {
            // Run this once
            if(!isPlay)
            {
                cameraMain.enabled = true;
                camerahat.enabled = false;
                pa.movementEnabled(true);
                isPlay = true;
            }

            distance = Vector3.Distance(playerPos.transform.position, transform.position);
            checkTimer -= Time.deltaTime;
            // Give player some time to escape
            if (checkTimer <= 0)
            {
                direction = playerPos.position - transform.position;
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), 0.3f);
            }

            // Within range, do melee attack
            if (distance < 10)
            {   
                meleeTimer -= Time.deltaTime;
                // Charge towards player
                if (meleeTimer <= 1)
                {
                    transform.position += transform.forward * 35 * Time.deltaTime;
                }
                else
                {
                    // Reinit timers, reset behavior
                    if (meleeTimer <= 0)
                    {
                        transform.position += Vector3.zero;
                        meleeTimer = 3.0f;
                        checkTimer = 1.0f;
                    }
                }
            }
            // Range attack, when out of range
            else
            {
                if (distance >= 10 && distance < 11)
                {
                    rangeTimer -= Time.deltaTime;
                    // Shoot laser
                    if (rangeTimer <= 1)
                    {
                        Ray ray = new Ray(transform.position, direction);
                        RaycastHit hitInfo;
                        if (Physics.Raycast(ray, out hitInfo))
                        {
                            Debug.DrawLine(ray.origin, hitInfo.point);
                            GameObject gameObj = hitInfo.collider.gameObject;
                            if (gameObj.tag == "Player")
                            {
                                Debug.Log(gameObj.name);
                            }
                        }
                    }
                    else
                    {
                        // Reinit timers, reset behavior
                        if (rangeTimer <= 0)
                        {
                            rangeTimer = 2.0f;
                            checkTimer = 1.0f;
                        }
                    }
                }
            }
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Player")
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(new Vector3(0, 0, 0)), 0.3f);
            pa.damage(meleeDmg);
        }
    }

    void OnCollisionExit(Collision other)
    {
        //other
    }

    void rangeAtk()
    {
        // Break off from update on final
    }
}