﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HostageEvent2 : MonoBehaviour
{
    Camera camera1;
    Camera camera2;
    float timer = 34.0f;
    GameObject hostage;
    GameObject terrorist;
    float distance;
    Transform player;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;//find player

    }
    // Start is called before the first frame update
    void Start()
    {
        hostage = GameObject.Find("Hostage2");
        /* camera1 = player.GetComponent<Camera>();
         camera2 = GetComponent<Camera>();
         camera2.enabled = false;

         terrorist = GameObject.Find("Terrorist");*/
    }

    // Update is called once per frame
    void Update()
    {
        distance = Vector3.Distance(player.transform.position, transform.position);
        if (distance < 100)
        {
            timer -= Time.deltaTime;
            if (timer <= 4)//change camera to hostages
            {
                /*camera1.enabled = false;
                camera2.enabled = true;*/
            }
            if (timer <= 2)//hostages die
            {
                //float step = Time.deltaTime;
                //terrorist.GetComponent<Rigidbody>().velocity = new Vector3(0, 1, 0);
                Destroy(hostage);
            }

            if (timer <= 0)//hostages die
            {
                /*camera1.enabled = true;
                camera2.enabled = false;*/
            }
        }
    }
}
