﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceDetector : MonoBehaviour
{
    private Transform player;

    // Use this for initialization

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;  // Find player
    }

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = player.transform.rotation; // Make the camera rotate with player
        transform.position = player.transform.position; // Make the camera go with player
        transform.Translate(Vector3.forward);   // Set it behind player
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag=="Enemy")
        {
            other.transform.Translate(10, 0, 10);
        }

    }

    void OnTriggerExit(Collider other)
    {
       //other
    }
}
