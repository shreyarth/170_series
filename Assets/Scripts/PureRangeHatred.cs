﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AI;

public class PureRangeHatred : MonoBehaviour
{
    float distance;
    GameObject ball;
    Transform player;
    int ranNumber;
    int ranNumberH;
    float ranNumberX;
    float ranNumberXH;
    float ranNumberZ;
    float ranNumberZH;
    Vector3 randomDirection;
    Vector3 dest;
    float timer = 3f;
    private Animator hat2_anim;
    public GameObject smooke;

    GameObject pPunch;
    NavMeshAgent agent;
    public int health = 10;

    // Start is called before the first frame update
    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;//find player
        hat2_anim = GetComponent<Animator>();
        pPunch = player.GetChild(3).gameObject;
        agent = GetComponent<NavMeshAgent>();
    }
    void Start()
    {
        InvokeRepeating("shoot", 1, 1);

        ranNumberH = Random.Range(0, 2);
        if (ranNumberH == 0)
        {
            ranNumberXH = -15;
        }
        else
        {
            ranNumberXH = 15;
        }

        ranNumberH = Random.Range(0, 2);
        if (ranNumber == 0)
        {
            ranNumberZH = -15;
        }
        else
        {
            ranNumberZH = 15;
        }
    }


    // Update is called once per frame
    void Update()
    {
        Vector3 direction = player.position - transform.position;//find direction to player
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), 0.3f);//Rotate to player

        distance = Vector3.Distance(player.transform.position, transform.position);//check distance

      //  smooke.transform.position=transform.position;

        timer -= Time.deltaTime;
        if (timer <= 2 && timer > 1.5f)
        {
            //set destination
            dest = new Vector3(player.position.x + ranNumberXH, player.position.y + 0.5f, player.position.z + ranNumberZH);
            //check whether it reaches destination
            // if (transform.position != dest)
            // {
            //     transform.position = Vector3.MoveTowards(transform.position, dest, 0.4f);
            // }
            // //stop there if reaching there
            // else
            // {
            //     transform.position += Vector3.zero;
            // }
            agent.SetDestination(dest);

        }

        //reset the random destination
        if (timer <= 0)
        {
            ranNumberH = Random.Range(0, 2);
            if (ranNumberH == 0)
            {
                ranNumberXH = -15;
            }
            else
            {
                ranNumberXH = 15;
            }

            ranNumberH = Random.Range(0, 2);
            if (ranNumber == 0)
            {
                ranNumberZH = -15;
            }
            else
            {
                ranNumberZH = 15;
            }
            timer = 3.0f;
        }

        //bound the hatred in the arena
        if(health <= 0)
        {
            SceneManager.LoadScene("Cutscene4");
        }
    }

    void shoot()
    {
        //check distance
        if (distance > 3)
        {
            hat2_anim.SetBool("atk_ran", true);
            ball = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            ball.GetComponent<Collider>().isTrigger = true;
            ball.transform.position = new Vector3(transform.position.x + 1, transform.position.y + 1, transform.position.z + 1);
            ball.gameObject.GetComponent<Renderer>().material = Resources.Load("tempbullets") as Material;//material
            ball.AddComponent<HatBullet>();
            ball.AddComponent<EnemyPunch>();
            ball.GetComponent<EnemyPunch>().damage = 25;

            GameObject smookey = (GameObject)Instantiate(smooke, ball.transform.position, ball.transform.rotation);
            smookey.transform.parent = ball.transform;
            smookey.transform.rotation = new Quaternion(smookey.transform.rotation.x, 75, 75, 0);
        }
        else
        {
            hat2_anim.SetBool("atk_ran", false);
        }
    }

    void OnCollisionEnter(Collision other)
    {
        // When collides with player punch, destroy
        // Debug.Log(punch);
        if (other.gameObject == GameObject.FindGameObjectWithTag("Player"))
        {
            if (pPunch.activeSelf is true)
            {
                health--;
                StopCoroutine(Dmg());
                StartCoroutine(Dmg());
            }
        }
    }

    IEnumerator Dmg()
    {
        for (int i = 0; i < 2; ++i)
        {

            gameObject.transform.GetChild(1).GetComponentInChildren<SkinnedMeshRenderer>().material.color = Color.green;
            gameObject.transform.GetChild(2).GetComponentInChildren<SkinnedMeshRenderer>().material.color = Color.green;
            yield return new WaitForSeconds(0.1f);
            gameObject.transform.GetChild(1).GetComponentInChildren<SkinnedMeshRenderer>().material.color = Color.yellow;
            gameObject.transform.GetChild(2).GetComponentInChildren<SkinnedMeshRenderer>().material.color = Color.yellow;
            yield return new WaitForSeconds(0.1f);
        }
    }
}
