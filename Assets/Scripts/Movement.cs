﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Movement : MonoBehaviour
{
    #region Character Controller
    //CHARACTER CONTROLLER----------------------------------
    //Make these private once the values are set
    public float rotateSpeed;
    public float jumpHeight = 8.0f;
    public float gravity = 20.0f;
    public float speed;
    public float superSpeed;
    private float horizontal;
    private float vertical;
    private Vector3 moveDirection = Vector3.zero;
    private CharacterController playerController;
    private int doubleJump;
    //------------------------------------------------------------
    #endregion
    
    //Player components
    private Rigidbody rig;
    private Animator anim;
    public bool goff;

    #region Audio
    //Audio Stuff
    public AudioSource source;
    public AudioClip steps;
    public AudioClip pewpew;
    public AudioClip flight;
    #endregion

    public static float pow = 0;
    public float powvalue;
    // Fetch char attribute script
    PlayerAttributes cattr;
    private float timer = 0f;

   

    // Start is called before the first frame update
    void Start()
    {
        rig = GetComponent<Rigidbody>();
        playerController = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();
        cattr = GetComponent<PlayerAttributes>();
        source = GetComponent<AudioSource>();
        goff = false;
        //rig.constraints = RigidbodyConstraints.FreezeRotation;
        powvalue = pow;
        
    }

    // Update is called once per frame
    void Update()
    {
        //Something might go here
    }

    //CHARACTER CONTROLLER ---------------------------------------------------------------------------------------------
    void FixedUpdate()
    {
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");

        float horizontalTurn = Input.GetAxis("Mouse X") * rotateSpeed;
        transform.Rotate(0, horizontalTurn, 0);

        // player jumping
        if (goff == false)
        {
            Move();
        }
        //anim.SetBool("walk", false);
        
        timer += 0.01f;
        if(timer > 6)
        {
            anim.SetBool("idle_change", true);
            timer = 0f;
            StartCoroutine("Idle_Change");
        }
    }

    ////fn for calling the step event from the animation for footsteps sound
    //private void step()
    //{
    //    source.PlayOneShot(steps, 1F);
    //}

    void Move()
    {
        if (playerController.isGrounded) //single jump
        {
            moveDirection = new Vector3(horizontal, 0, vertical);
            moveDirection = transform.TransformDirection(moveDirection); //localize direction to be player character
            
            if(Input.GetButton("Superspeed"))
            {
                moveDirection *= superSpeed;
            }
            else
            {
                moveDirection *= speed;
            }
            //anim.SetBool("on_g", true);
            anim.SetBool("walk", true);
            //gets X and Y for animations of running, strafing, walking
            anim.SetFloat("velo_x", horizontal);
            anim.SetFloat("velo_y", vertical);
            if (Input.GetButtonDown("Jump"))
            {
                StartCoroutine("Jump_Anim");
                anim.SetBool("jump", true); //animation for jump    
            }
            else
            {
                anim.SetBool("jump", false);
            }
            doubleJump = 0;
        }
        else //allows you to jump and move
        {
            moveDirection = new Vector3(horizontal, moveDirection.y, vertical);
            moveDirection = transform.TransformDirection(moveDirection); //localize direction to be player character
            if(Input.GetButton("Superspeed"))
            {
                moveDirection.z *= superSpeed;
                moveDirection.x *= superSpeed;
            }
            else
            {
                moveDirection.z *= speed;
                moveDirection.x *= speed;
            }
            //air animation after first jump
            //anim.SetBool("is_Space_Pressed", false);

            //double jump
            if (Input.GetButtonDown("Jump") && doubleJump < 1)
            {
                moveDirection.y = jumpHeight;
                //anim.SetBool("on_g", false);
                doubleJump++;
            }
        }

        //move character
        moveDirection.y -= gravity * Time.deltaTime;
        playerController.Move(moveDirection * Time.deltaTime);
    }
    public void LosePow()
    {
        pow += 1;
    }

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        //Debug.Log("nani");
        if (hit.gameObject.CompareTag("Death"))
        {
            cattr.damage(200);
        }
    }
    /*
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            StopAllCoroutines();
            StartCoroutine("Pow");
        }
        if (collision.gameObject.CompareTag("Boss"))
        { 
            StopAllCoroutines();
            StartCoroutine("Pow");
        }
    }
    */
    IEnumerator Jump_Anim()
    {
        yield return new WaitForSeconds(0.01f);
        moveDirection.y = jumpHeight;
        playerController.Move(moveDirection * Time.deltaTime);
    }

    IEnumerator Idle_Change()
    {
        yield return new WaitForSeconds(3f);
        anim.SetBool("idle_change", false);
    }

    void Step()
    {
        if (playerController.isGrounded)
        {
            source.PlayOneShot(steps, 0.5f);
        }
    }
}
