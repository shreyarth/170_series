﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class pauseMenu : MonoBehaviour
{
    // public CanvasGroup group;
    public GameObject menuLayer;
    public GameObject lightObj;
    public GameObject camera;

    Pause pause;

    // Start is called before the first frame update
    void Awake()
    {
    	pause = lightObj.GetComponent<Pause>();
    	camera.GetComponent<CameraMovement>();
        gameObject.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            resumeGame();
        }
    }

    void OnGUI()
    {
        if (!pause.isPaused())
            return;
    }

    void cameraRelease()
    {
    	camera.GetComponent<CameraMovement>().enabled = true;
    	Cursor.lockState = CursorLockMode.Locked;
    	Cursor.visible = false;
    }

    public void resumeGame()
    {
    	pause.setPaused(false);
        Time.timeScale = 1;
        cameraRelease();
        gameObject.SetActive(false);
    }

    public void quitGame()
    {
    	Time.timeScale = 1;
    	// Application.Quit();
        SceneManager.LoadScene("MenuScene");
    }
}
