using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class textbox : MonoBehaviour
{
	// public CanvasGroup group;
    public GameObject panel;
	public Text txt;
	private float sec; // Lifespan of the textbox

    GameObject player;

    // Start is called before the first frame update
    void Awake()
    {
        gameObject.SetActive(false);
    }

    void Start()
    {

    }

    void OnEnable()
    {
        Invoke ("Destroy", sec);
    }

    // Update is called once per frame
    void Update()
    {	

    }

    // Instead of destroying the object, set it inactive
    void Destroy()
    {
        gameObject.SetActive(false);
    }

    // Don't call invoke when inactive
    void OnDisable()
    {
        CancelInvoke();
    }

    // Call to activate the textbox for t secs
    // t: lifespan
    // pos: position of caption box (based on pivot point aka center of the screen)
    public void callCaption (string str, float t, Vector2 pos)
    {
    	txt.text = str;
    	sec = t;

        panel.GetComponent<RectTransform>().anchoredPosition = pos;
    }
}
