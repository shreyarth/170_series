﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class startScene : MonoBehaviour
{
    public void loadByIdx (int idx)
    {
    	SceneManager.LoadScene(idx);
    }

    public void loadByStr (string sceneName)
    {
    	SceneManager.LoadScene(sceneName);
    }

    public void quitGame ()
    {
    	Application.Quit();
    }
}
