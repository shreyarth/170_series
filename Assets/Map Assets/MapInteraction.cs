﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MapInteraction : MonoBehaviour
{
    private static float play = 1;


    public void ConstructionSceneSelect()
    {
        if (play == 1)
        {
            SceneManager.LoadScene("Cutscene1");
            play--;
        }
        else
            SceneManager.LoadScene("ReplayConstruction");
    }

    public void BankSceneSelect()
    {
        SceneManager.LoadScene("BankScene");
    }
    public void StadiumSceneSelect()
    {
        SceneManager.LoadScene("Stadium");
    }
    public void StatueSceneSelect()
    {
        SceneManager.LoadScene("StatueScene");
    }
}
